from django.conf import settings
from house_shop.models import House

class Basket:
    def __init__(self, request):
        self.session = request.session
        basket = self.session.get(settings.BASKET_SESSION_ID)
        if not basket:
            basket = self.session[settings.BASKET_SESSION_ID] = {}
        self.basket = basket

    def __iter__(self):
        houses_id = self.basket.keys()
        house_list = House.objects.filter(pk__in=houses_id)
        basket = self.basket.copy()
        for house in house_list:
            basket[str(house.id)]['house'] = house
        for item in basket.values():
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        return sum(item['quantity'] for item in self.basket.values())

    def save(self):
        self.session[settings.BASKET_SESSION_ID] = self.basket
        self.session.modified = True

    def add(self, house, quantity=1):
        house_id = str(house.id)
        if house_id not in self.basket:
            self.basket[house_id] = {
                'quantity': 0,
                'price': house.price
            }
        self.basket[house_id]['quantity'] += quantity
        self.save()

    def remove(self, house):
        house_id = str(house.id)
        if house_id in self.basket:
            del self.basket[house_id]
            self.save()

    def get_total_price(self):
        return sum(item['price'] * item['quantity'] for item in self.basket.values())

    def clear(self):
        del self.session[settings.BASKET_SESSION_ID]
        self.session.modified = True
