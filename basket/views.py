from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from house_shop.models import House, Order, Pos_order
from .basket import Basket
from .forms import BasketAddProductForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.conf import settings


@require_POST
def basket_add(request, house_id):
    basket = Basket(request)
    house = get_object_or_404(House, pk=house_id)
    form = BasketAddProductForm(request.POST)
    if form.is_valid():
        basket.add(
            house=house,
            quantity=form.cleaned_data['quantity']
        )
    return redirect('basket')


def basket_remove(request, house_id):
    basket = Basket(request)
    house = get_object_or_404(House, pk=house_id)
    basket.remove(house)
    messages.success(request, 'Товар удален')
    return redirect('basket')


def basket_clear(request):
    basket = Basket(request)
    basket.clear()
    messages.success(request, 'Корзина очищена')
    return redirect('basket')


def basket_detail(request):
    basket = Basket(request)
    return render(request,'basket/detail.html', context={'basket': basket})


@login_required
def basket_buy(request):
    basket = Basket(request)
    if basket.__len__() <= 0:
        messages.warning(request, 'Корзина пуста')
        return redirect('basket')
    FIO_customer = request.user.username
    order = Order.objects.create(FIO_customer=FIO_customer)
    order.price = basket.get_total_price()
    for item in basket:
        pos_order = Pos_order.objects.create(
            house=item['house'],
            count=item['quantity'],
            order=order
        )
    basket.clear()
    messages.success(request, 'Заказ успешно создан')
    return redirect('basket')
