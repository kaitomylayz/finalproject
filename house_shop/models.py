from  django.db import models
from django.urls import reverse_lazy

MAX_LENGTH = 256

class Supplier(models.Model):
    name = models.CharField(max_length=MAX_LENGTH, verbose_name='Название компании поставщика')
    agent_firstname = models.CharField(max_length=MAX_LENGTH, verbose_name='Фамилия агента')
    agent_name = models.CharField(max_length=MAX_LENGTH, verbose_name='Имя агента')
    telephone = models.CharField(max_length=MAX_LENGTH, verbose_name='Телефон компании')
    city = models.CharField(null=True, blank=True, verbose_name='Город отгрузки')
    address = models.CharField(null=True, blank=True, verbose_name='Адрес поставщика')
    is_exists = models.BooleanField(default=True, verbose_name='Возможность заказа')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Поставщик'
        verbose_name_plural = 'Поставщики'

class Developer(models.Model):
    name = models.CharField(max_length=MAX_LENGTH, verbose_name='Название застройщика')
    description = models.TextField(null=True, blank=True, verbose_name='Описание застройщика')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Застройщик'
        verbose_name_plural = 'Застройщики'

class Services(models.Model):
    name = models.CharField(max_length=MAX_LENGTH, verbose_name='Название услуги для дома')
    description = models.TextField(null=True, blank=True, verbose_name='Описание услуги')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

class House(models.Model):
    name = models.CharField(max_length=MAX_LENGTH, verbose_name='Название дома')
    descriptions = models.TextField(null=True, blank=True, verbose_name='Описание дома')
    price = models.FloatField(verbose_name='Цена')
    size = models.CharField(max_length=MAX_LENGTH, verbose_name='Размер')
    color = models.CharField(max_length=MAX_LENGTH, verbose_name='Цвет')
    type = models.CharField(max_length=MAX_LENGTH, verbose_name='Тип')
    photo = models.ImageField(upload_to='image/%Y/%m/%d', null=True, blank=True, verbose_name='Фото объекта')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания записи')
    update_date = models.DateTimeField(auto_now=True, verbose_name='Дата последнего изменения записи')
    is_exists = models.BooleanField(default=True, verbose_name='Возможность заказа')

    developer = models.ForeignKey(Developer, on_delete=models.PROTECT, verbose_name='Застройщик')
    service = models.ManyToManyField(Services, blank=True, verbose_name='Услуга')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Дом'
        verbose_name_plural = 'Дома'

class Supply(models.Model):
    data_supply = models.DateTimeField(verbose_name='Дата поставки')
    supplier = models.ForeignKey(Supplier, on_delete=models.PROTECT, verbose_name='Поставщик')
    house = models.ManyToManyField('House', through='Pos_supply', verbose_name='Дом')

    def __str__(self):
        return f'#{self.pk} - {self.data_supply} {self.supplier.name}'

    class Meta:
        verbose_name = 'Поставка'
        verbose_name_plural = 'Поставки'

class Order(models.Model):
    FIO_customer = models.CharField(max_length=MAX_LENGTH, verbose_name='ФИО заказчика')
    comment = models.CharField(blank=True, null=True, verbose_name='Комментарий к заказу')
    building_time_start = models.DateTimeField(auto_now_add=True, verbose_name='Дата начала строительства')
    building_time_finish = models.DateTimeField(verbose_name='Дата окончания строительства')
    key_time = models.DateTimeField(verbose_name='Дата выдачи ключей')

    house = models.ManyToManyField('House', through='Pos_order', verbose_name='Дом')

    def __str__(self):
        return f'#{self.pk} {self.FIO_customer} - {self.house.name}'

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class Pos_supply(models.Model):
    house = models.ForeignKey(House, on_delete=models.PROTECT, verbose_name='Дом')
    supply = models.ForeignKey(Supply, on_delete=models.PROTECT, verbose_name='Поставка')

    count = models.PositiveIntegerField(default=1, verbose_name='Количество')

    def __str__(self):
        return f'{self.house.name} - #{self.supply.pk}'

    class Meta:
        verbose_name = 'Позиция поставки'
        verbose_name_plural = 'Позиции поставок'

class Pos_order(models.Model):
    house = models.ForeignKey(House, on_delete=models.PROTECT, verbose_name='Дом')
    order = models.ForeignKey(Order, on_delete=models.PROTECT, verbose_name='Заказ')
    count = models.PositiveIntegerField(default=1, verbose_name='Количество')
    discount = models.PositiveIntegerField(default=0, verbose_name='Скидка на дом')

    def __str__(self):
        return f'{self.pk} {self.house.name} {self.order.FIO_customer}'

    class Meta:
        verbose_name = 'Позиция заказа'
        verbose_name_plural = 'Позиции заказа'
