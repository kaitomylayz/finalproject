from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag(name='company')
def name_company():
    return settings.NAME_COMPANY


@register.simple_tag(name='active')
def active_class(current_page, expected_page):
    if current_page == expected_page:
        return 'active'
