from django.urls import path, include

from .views import *

urlpatterns = [
    path('', home, name='home_page'),

    path('catalog/', HouseList.as_view(), name='house_list_page'),
    path('catalog/<int:pk>/', HouseDetail.as_view(), name='house_detail_page'),

    path('developer/', DeveloperList.as_view(), name='developer_list_page'),
    path('developer/<int:pk>/', DeveloperDetail.as_view(), name='developer_detail_page'),

    path('order/', OrderList.as_view(), name='order_list_page'),
    path('order/create/', OrderCreate.as_view(), name='order_create_page'),
    path('order/<int:pk>/', OrderDetail.as_view(), name='order_detail_page'),

    path('supplier/', SupplierList.as_view(), name='supplier_list_page'),
    path('supplier/<int:pk>/', SupplierDetail.as_view(), name='supplier_detail_page'),

    path('login/', login_user, name='login_page'),
    path('registration/', registration_user, name='registration_page'),
    path('logout/', logout_user, name='logout_page'),
]