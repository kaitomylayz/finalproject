from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator

from .models import *
from .forms import *


# ------------------ Index ----------------------

def home(request):
    return render(request, 'house_shop/index.html')


def login_user(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            if request.GET.get('next'):
                return redirect(request.GET.get('next'))
            return redirect('home_page')
    else:
        form = AuthenticationForm()
    context = {
        'form': form
    }
    return render(request, 'house_shop/auth/login.html', context)


def registration_user(request):
    if request.method == 'POST':
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            login(request, form.save())
            if request.GET.get('next'):
                return redirect(request.GET.get('next'))
            return redirect('home_page')
    else:
        form = UserCreationForm()
    context = {
        'form': form
    }
    return render(request, 'house_shop/auth/registration.html', context)


def logout_user(request):
    logout(request)
    return redirect('home_page')


# ------------------- House ----------------------

class HouseList(ListView):
    model = House
    template_name = 'house_shop/house/list.html'
    allow_empty = True
    paginate_by = 6

    def get_queryset(self):
        return House.objects.filter(is_exists=True)


class HouseDetail(DetailView):
    model = House
    template_name = 'house_shop/house/detail.html'


# ------------------- Developer ----------------------

class DeveloperList(ListView):
    model = Developer
    template_name = 'house_shop/developer/list.html'
    allow_empty = True
    paginate_by = 10


class DeveloperDetail(DetailView):
    model = Developer
    template_name = 'house_shop/developer/detail.html'


# ------------------- Order ----------------------

class OrderList(ListView):
    model = Order
    template_name = 'house_shop/order/list.html'
    allow_empty = True
    paginate_by = 12


class OrderCreate(CreateView):
    model = Order
    template_name = 'house_shop/order/detail.html'
    form_class = OrderForm


class OrderDetail(DetailView):
    model = Order
    template_name = 'house_shop/order/detail.html'


# ------------------- Supplier ----------------------


class SupplierList(ListView):
    model = Supplier
    template_name = 'house_shop/supplier/list.html'
    form_class = OrderForm


class SupplierDetail(DetailView):
    model = Supplier
    template_name = 'house_shop/supplier/detail.html'
